<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <title>Signup</title>
  </head>
  <body>
    <h1 class="text-center text-success mt-3 mb-5 pb-5 " MX-3>Tokopedia</h1>

    {{-- FORM --}}
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-md-4 mr-md-5">
                <img src="https://ecs7.tokopedia.net/img/content/register_new.png" width="100%" alt="">
                    <h5>Jual Beli Mudah Hanya di Tokopedia</h5>
                    <p class="text-center">Gabung dan rasakan kemudahan bertransaksi di Tokopedia</p>
            </div>

            {{-- kanan --}}
            <div class="col-md-4 ml-md-5">
                <div class="shadow p-3 mb-5 bg-white rounded">
                    <h2 class="text-center">Daftar Sekarang</h2>
                    <p class="text-center">Sudah punya akun Tokopedia? <a href="">Masuk</a></p>
                    {{-- form --}}
                    <form class="form-inline justify-content-center" action="/welcome" method="POST">
                        @csrf
                        <div class="form-group mt-4 mb-3">
                            <input type="text" name="Fname" class="form-control mx-sm-3 px-5" placeholder="First name">
                        </div>
                        <div class="form-group mb-2">
                          <input type="text" name="Lname" class="form-control mx-sm-3 px-5" placeholder="last name">
                        </div>
                        <div class="form-group mb-2">
                          <input type="email" name="email" class="form-control mx-sm-3 px-5" placeholder="youremail@azizgans.com">
                        </div>
                        <div class="form-group mb-4">
                          <input type="password" name="password" class="form-control mx-sm-3 px-5" placeholder="Password">
                        </div>

                        <button type="submit" class="btn btn-success btn-lg btn-block">Daftar</button>
                            <div class="my-3" style="font-size:0.75em">
                                <div>Dengan mendaftar, saya menyetujui</div>
                                <a href="" class="text-decoration-none text-success">Syarat dan Ketentuan</a> 
                                <span> serta </span>
                                <a href="" class="text-decoration-none text-success">Kebijakan Privasi</a>
                            </div>
                        </p>
                      </form>
                </div>
            </div>
        </div>
    </div>












    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
{{-- end js botstrap --}}
  </body>
</html>